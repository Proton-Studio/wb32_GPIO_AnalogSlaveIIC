#ifndef __IIC_H
#define __IIC_H
#include "wb32f10x_it.h"

#define SLAVEID 0xA0   //IIC slave device address

#define SDA_IN  1
#define SDA_OUT 0

#define sI2C_SCL_RCC_Periph      RCC_APB1Periph_GPIOB   //SCL GPIO clock
#define sI2C_SDA_RCC_Periph      RCC_APB1Periph_GPIOB   //SDA GPIO clock

#define sI2C_SDA_GPIO GPIOB
#define sI2C_SDA_PIN  GPIO_Pin_0

#define sI2C_SCL_GPIO GPIOB
#define sI2C_SCL_PIN  GPIO_Pin_1

#define sI2C_GET_SCL  PBin(1)   // Define SCL_Pin in bit band 
#define sI2C_GET_SDA  PBin(0)   // Define SDA_Pin in bit band 

#define sI2C_SET_SCL0  PBout(1) // Define SCL_Pin out bit band 
#define sI2C_SET_SDA0  PBout(0) // Define SDA_Pin out bit band 

#define sI2C_SET_SDA(level) {sI2C_DIR_SDA(SDA_OUT);sI2C_SET_SDA0=level;}  // Set SDA_Pin output level

typedef enum
{
    sI2C_STATE_NA = 0,   //NA
    sI2C_STATE_STA,      //START
    sI2C_STATE_ADD,      //ADD
    sI2C_STATE_ADD_ACK,  //ADD ACK
    sI2C_STATE_DAT,      //DATA 
    sI2C_STATE_DAT_ACK,  //DATA ACK
    sI2C_STATE_STO       //STOP
}sI2C_STATE;

                        
extern void IIC_Config(void);    
 
#endif

