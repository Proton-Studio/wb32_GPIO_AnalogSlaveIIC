#ifndef __SYSTICK_H
#define __SYSTICK_H
#include "wb32f10x_it.h"

extern void SysTick_Init(void);
extern void delay_us(__IO uint32_t nTime);
extern void delay_ms(__IO uint32_t nTime);
 
#endif
