#ifndef __LED_H
#define __LED_H
#include "wb32f10x_it.h"

#define ON  0
#define OFF 1

#define LED1 PBout(13)
#define LED2 PBout(14)

extern void LED_Init(void);
 
#endif
