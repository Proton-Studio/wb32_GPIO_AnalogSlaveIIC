#include "LED.h"

void LED_Init(void)
{
  RCC->APB1ENR|=(1<<15)|(1<<6);   //使能BMX1 //使能GPIOB时钟  
  
  GPIOB->CFGMSK =~((1<<13)|(1<<14)); //使能对应管脚配置 对应位置0可配置
  
  GPIOB->MODER = 0x1 * 0x55555555U ; //复用功能模式  10
  
  GPIOB->OTYPER =  0 * 0xFFFFFFFFU;   //推挽输出 0
  
  GPIOB->OSPEEDR = 0 * 0x55555555U ;   // 高速 00
  
  GPIOB->PUPDR =   0 * 0x55555555U ;   // 无上拉无下拉 00  
  
  GPIOB->AFRL = 0;  //无复用
  GPIOB->AFRH = 0;  //无复用

  LED1 = OFF;  //关闭LED1
  LED2 = OFF;  //关闭LED2
  
  return ;
}




