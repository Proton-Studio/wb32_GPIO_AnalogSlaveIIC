#include "EXTI.h"
#include "IIC.h"



/**
  * @brief  Configure EXTI_GPIO working mode
  * @param  None
  * @return None
  */
 void EXTI_GPIO_Init(void)
 {
   /* Enable GPIOA clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BMX1 |sI2C_SCL_RCC_Periph | sI2C_SDA_RCC_Periph, ENABLE);
   /* 配置为开漏输出，必须要接上拉电阻。在开漏模式时，对输入数据寄存器的读访问可得到I/O状态 */
  GPIO_Init(sI2C_SCL_GPIO, sI2C_SCL_PIN , GPIO_MODE_IN |GPIO_OTYPE_OD);
  GPIO_Init(sI2C_SDA_GPIO, sI2C_SDA_PIN , GPIO_MODE_IN |GPIO_OTYPE_OD);
 }

/**
  * @brief  Configure PA0 in interrupt mode
  * @param  None
  * @return None
  */
void EXTI_Config(void)
{
  /* Private variables ---------------------------------------------------------*/
  EXTI_InitTypeDef   EXTI_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  EXTI_GPIO_Init();

  /* Enable AFIO clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_BMX1 |RCC_APB1Periph_EXTI |RCC_APB1Periph_AFIO, ENABLE);

  /* Connect EXTI Line to SDA pin */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource0 );
  
  /* Connect EXTI Line to SCL pin */
  GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource1);

  /* Configure EXTI line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line0;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Configure and enable EXTI interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  
    /* Configure EXTI line */
  EXTI_InitStructure.EXTI_Line = EXTI_Line1;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Configure and enable EXTI interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

    /* Generate software interrupt: simulate a falling edge applied on EXTI0 line */
  EXTI_GenerateSWInterrupt(EXTI_Line0);
  
  /* Generate software interrupt: simulate a falling edge applied on EXTI1 line */
  EXTI_GenerateSWInterrupt(EXTI_Line1);
}

void Exti_Init(void)
{
  /* Configure interrupt mode */
  EXTI_Config();
}





