/* Includes ------------------------------------------------------------------*/
#include "wb32f10x.h"
#include "SysTick.h"
#include "timer.h"
#include "EXTI.h"
#include "LED.h"
#include "IIC.h"
#include "drv_eeprom_24c02.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint32_t errCode = 0;
uint32_t result = 0;
uint8_t rdata1 = 0;
uint8_t rdata2 = 0;
uint8_t rd_buffer[256];
uint8_t wr_buffer[256]={0x01,0x02,0x03,0x04,0x05,0x66,0x77,0x88,0x99};
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


int main(void)
{
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//中断优先级分组2 
  TIM3_Int_Init(1,95);
  SysTick_Init(); //滴答定时器初始化
  IIC_Config();   //IIC配置
  LED_Init();     //LED初始化

  /* Initialize the 24C02 driver */
  eeprom_24c02_init();
  

  /* Infinite loop */
  while (1)
  {    
    rdata1=~rdata1;

    eeprom_24c02_page_write(0, wr_buffer + rdata1, 8);
    
    LED1 = ON;
    LED2 = OFF;
    
    delay_ms(150);
    
    eeprom_24c02_sequential_read(0x01,rd_buffer,8);
    
    LED1 = OFF;
    LED2 = ON;
    
    delay_ms(150);  
    
  }
}



#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
